Collaborative design workshop - CloudForms, Jobs & Tasks

We are SO excited about this! We are currently working on a complete redesign for jobs & tasks, and we would love to get your feedback here. 

Do you want to shape the user experience of CloudForms? Are you familiar with requests, jobs and tasks in CF? Join us for a design workshop where we will work together in an interactive session. We want to hear your feedback on how the product is used. We will talk about personas, empathy maps, looking at pains and gains and validate a prototype! 

[Slides](https://docs.google.com/presentation/d/1cUtu3Lrr1AnG7_jknRk1ZQs0JWPRp9EoplEqMOGWcqk/edit?usp=sharing)

[Share your feedback with us!](https://docs.google.com/forms/d/e/1FAIpQLSf--iFwF2LRkgic0nbiD__invMxLd1QUWRYO0vK9HsL4B08YA/viewform?usp=pp_url)