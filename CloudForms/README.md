![cloud-computing](img/cloud-computing.jpg)

# CloudForms

# How Do I Access the Application/Environment?

Please ask any of your hackathon leaders for the credentials to access the environment.

# How Do I Access the API?

CloudForms® APIs are accessed using `/api` at the end of your login URL. Please note that you need to supply your credentials again. 

# API Documentation

You can find the API documentation at:

* https://access.redhat.com/documentation/en-us/red_hat_cloudforms/4.7/html/red_hat_cloudforms_rest_api/index
* http://manageiq.org/docs/reference/fine/api/examples/queries
* http://manageiq.org/docs/reference/euwe/doc-REST_API/miq/index

# Tracks

[Runbook for Demos](track1.md)

[Reports, Control Policies, Alerts - Build and Document Examples](track2.md)

[Extend CloudForms Capabilities Via Ansible Playbooks](track3.md)

[Working Session About the New Policies Editor and Task/Jobs Display](track4.md)

[Build Workshops](track5.md) 

You are welcome to create your own track. Please talk to any of the hackathon leaders about it and submit a PR.

# Feedback

Please provide constructuve feedback about the UX via this [Form](https://docs.google.com/forms/d/e/1FAIpQLSf--iFwF2LRkgic0nbiD__invMxLd1QUWRYO0vK9HsL4B08YA/viewform?usp=pp_url
)