# Build Workshops

We want to explore the idea of creating CloudForms workshops similar to the ones that exist for Ansible. 

Anant Dhar will start working on this, please reach him adhar@redhat.com  if you want to contribute here 

For reference, please visit https://github.com/ansible/workshops.