# Scripting for Reporting
We currently expose the data collected and processed by Cost Management via the API, and you can export this data to a CSV file.

We expect you to use the API or the CSV file to create reports of the actual consumption and identify any missing information.