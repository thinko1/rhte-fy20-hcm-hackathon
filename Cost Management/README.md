![cost](img/cost.jpg)

# Cost Management

# How Do I Access the Application/Environment?

Please ask any of your hackathon leaders for the credentials to access the environment.

# How Do I Access the API? 

You need to have a valid username and password to access `cloud.redhat.com`, and the user must have the correct entitlements attached. 

To get your credentials to continue working on this project after the hackathon, please contact [Victor Estival](mailto:vestival@redhat.com), [Sergio Ocon](socon@redhat.com), or [Steve Fulmer](sfulmer@redhat.com).

Once you have validated your user, you can access the API at
[https://cloud.redhat.com/api/cost-management/v1/](https://cloud.redhat.com/api/cost-management/v1/).

# API Documentation

The API is documented using [Open API](https://www.openapis.org/). You can find the full documentation [here](https://github.com/project-koku/koku/blob/master/docs/source/specs/openapi.json).

You can open the file in [https://editor.swagger.io/](https://editor.swagger.io/) to make it much more convenient to browse.



# Tracks
[API Harvesting for Meaningful Outputs](track1.md)

[Scripting for Reporting](track2.md)

[Generate Services Around the Product](track3.md)

[Mock Review/Feedback](track4.md)

You are welcome to create you own track. Please talk to any of the hackathon leaders about it and submit a PR.

# Feedback

Please provide constructive feedback about the tool, your experience, and any possible improvements via the provided [form](https://forms.gle/C7usv4A1XPvKjCmN7).